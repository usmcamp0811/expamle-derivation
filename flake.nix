{
  description = "A very basic flake";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable"; };

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      neovim-config = pkgs.stdenv.mkDerivation {
        name = "dotfiles";
        src = ./.config/nvim;
        installPhase = ''
          mkdir -p $out
          cp -r $src $out/src
        '';
      };
      qtile-config = pkgs.stdenv.mkDerivation {
        name = "dotfiles";
        src = ./.config/qtile;
        installPhase = ''
          mkdir -p $out
          cp -r $src $out/qtile
        '';
      };
      default = pkgs.stdenv.mkDerivation {
        name = "dotfiles";
        src = ./.;
        installPhase = ''
          mkdir -p $out
          cp -r ${qtile-config} $out/qtile
          cp -r ${neovim-config} $out/nvim
        '';
      };
    in {

      packages.x86_64-linux.default = default;
      packages.x86_64-linux.nvim = neovim-config;
      packages.x86_64-linux.qtile = qtile-config;

    };
}
