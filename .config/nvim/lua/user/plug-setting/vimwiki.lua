
vim.cmd "autocmd BufRead,BufNewFile *.wiki set filetype=vimwik"
-- vim.cmd "autocmd BufRead,BufNewFile diary.md :CalendarVR"
vim.cmd "autocmd BufReadPre diary.md :VimwikiDiaryGenerateLinks"
-- vim.cmd "autocmd BufReadPre index.md :VimwikiGenerateLinks"

vim.g.vimwiki_ext2syntax = {
  [".Rmd"] = "markdown",
  [".rmd"] = "markdown",
  [".markdown"] = "markdown",
  [".mdown"] = "markdown",
  [".md"] = "markdown"
}
vim.g.vimwiki_list = {
  {
      path = '~/vimwiki',
      syntax = 'markdown',
      ext = '.md',
      auto_toc = 1,
      auto_tags = 1,
      links_space_char = "_",
      auto_diary_index = 1,
      diary_caption_level = 2,
  },
  {
      path = '~/vimwiki/devops',
      syntax = 'markdown',
      ext = '.md',
      auto_toc = 1,
      auto_tags = 1,
      links_space_char = "_",
  },
  {
      path = '~/vimwiki/code',
      syntax = 'markdown',
      ext = '.md',
      auto_toc = 1,
      auto_tags = 1,
      links_space_char = "_",
  },
  {
      path = '~/vimwiki/linux',
      syntax = 'markdown',
      ext = '.md',
      auto_toc = 1,
      auto_tags = 1,
      links_space_char = "_",
  },
  {
      path = '~/vimwiki/random',
      syntax = 'markdown',
      ext = '.md',
      auto_toc = 1,
      auto_tags = 1,
      links_space_char = "_",
  }


}
vim.g.vim_markdown_math = 1
vim.g.vim_markdown_json_frontmatter = 1
vim.g.vim_markdown_strikethrough = 1
vim.g.vim_markdown_new_list_item_indent = 2
vim.g.vimwiki_global_ext = 0
vim.g.vimwiki_table_mappings = 0
vim.g.vimwiki_listsyms = '✗○◐●✓'
vim.g.vimwiki_use_calendar = 1
vim.g.vimwiki_auto_header = 1
vim.g.vimwiki_folding = 'custom'

local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
  return
end

which_key.register({
  W = {
    name = "VimWiki",
    d = { "<Plug>VimwikiIncrementListItem", "Incriment Completion Level"},
    u = { "<Plug>VimwikiDecrementListItem", "Incriment Completion Level"},
    t = { "<Plug>VimwikiToggleListItem", "Toggle Checkbox"},
    f = { '<cmd>lua require("telescope.builtin").find_files({cwd = "~/vimwiki"})<CR>', "Find Wiki" },
    D = { "<cmd>VimwikiDiaryIndex<cr>", "Vimwiki Diary" },
    w = { "<cmd>VimwikiIndex<CR>", "Vimwiki Main" },
    n = { "<cmd>VimwikiMakeDiaryNote<CR>", "Daily Note"},
    N = { "<cmd>VimwikiMakeTomorrowDiaryNote<CR>", "Tomorrows Note"},
  },
},
  { prefix = "<leader>" }
)

vim.g.vimwiki_filetypes = {'markdown', 'pandoc'}
