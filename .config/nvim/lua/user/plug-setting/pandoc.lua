vim.g["pandoc#syntax#codeblocks#embeds#langs"] = {"python", "julia", "bash=sh", "json=javascript", "javascript", "clojure", "zsh=sh", "dockerfile"}
vim.g["pandoc#syntax#conceal#urls"] = 1
-- vim.g["pandoc#folding#mode"] = {'manual'}
vim.g["pandoc#folding#fdc"] = 0

