return {
	settings = {
		python = {
			analysis = {
				indexing = true,
				typeCheckingMode = "basic",
				diagnosticMode = "openFilesOnly",
				inlayHints = {
					variableTypes = true,
					functionReturnTypes = true,
				},
				stubPath = vim.fn.expand("$HOME/typings"),
				diagnosticSeverityOverrides = {
					reportUnusedImport = "information",
					reportUnusedFunction = "information",
					reportUnusedVariable = "information",
				},
			},
		},
	},
}
